package com.example.preexac2_promovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.preexac2_promovil.Modelo.ProductosDb;

public class MainActivity extends AppCompatActivity {

    public ProductosDb productosDb;
    public EditText txtCodigo;
    public EditText txtNombre;
    public EditText txtMarca;
    public EditText txtPrecio;
    public Button btnNuevo;
    public Button btnGuardar;
    public Button btnLimpiar;
    public Button btnEditar;

    public RadioButton rdbPerecedero;
    public RadioButton rdbNoPerecedero;

    private boolean isEditing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productosDb = new ProductosDb(this);
        txtCodigo = findViewById(R.id.editCodigo);
        txtNombre = findViewById(R.id.editNombre);
        txtMarca = findViewById(R.id.editMarca);
        txtPrecio = findViewById(R.id.editPrecio);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnEditar = findViewById(R.id.btnEditar);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnNuevo = findViewById(R.id.btnNuevo);
        rdbPerecedero = findViewById(R.id.radioPerecedero);
        rdbNoPerecedero = findViewById(R.id.radioNoPerecedero);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
                enableFields();
                isEditing = false;
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoStr = txtCodigo.getText().toString();
                String nombre = txtNombre.getText().toString();
                String marca = txtMarca.getText().toString();
                String precioStr = txtPrecio.getText().toString();

                if (codigoStr.isEmpty() || nombre.isEmpty() || marca.isEmpty() || precioStr.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Favor de completar todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    int codigo = Integer.parseInt(codigoStr);
                    double precio = Double.parseDouble(precioStr);
                    boolean perecedero = rdbPerecedero.isChecked();

                    Producto producto = new Producto(codigoStr, nombre, marca, precioStr, -1, perecedero ? "Perecedero" : "No perecedero");

                    if (isEditing) {
                        long updatedRows = productosDb.updateProducto(producto);
                        if (updatedRows > 0) {
                            Toast.makeText(MainActivity.this, "Actualizado con éxito", Toast.LENGTH_SHORT).show();
                            clearFields();
                        } else {
                            Toast.makeText(MainActivity.this, "Ocurrió un error, inténtalo de nuevo", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        long insertedRow = productosDb.insertProducto(producto);
                        if (insertedRow > 0) {
                            Toast.makeText(MainActivity.this, "Guardado con éxito.", Toast.LENGTH_SHORT).show();
                            clearFields();
                        } else {
                            Toast.makeText(MainActivity.this, "Ocurrió un error, inténtalo de nuevo", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProductoItem.class);
                startActivity(intent);
            }
        });

    }

    private void clearFields() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        // Debes también limpiar el estado del RadioButton
    }
    private void enableFields() {
        txtCodigo.setEnabled(!isEditing);
        txtNombre.setEnabled(true);
        txtMarca.setEnabled(true);
        txtPrecio.setEnabled(true);
        // Habilita también el RadioButton
    }

}
