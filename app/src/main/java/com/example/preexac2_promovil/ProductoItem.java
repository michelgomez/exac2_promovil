package com.example.preexac2_promovil;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.preexac2_promovil.Modelo.ProductosDb;
public class ProductoItem extends AppCompatActivity {

    private ProductosDb productosDb;
    private EditText etCodigo;
    private EditText etNombre;
    private EditText etMarca;
    private EditText etPrecio;
    private RadioButton rdbPerecedero;
    private RadioButton rdbNoPerecedero;
    private Button btnBuscar;
    private Button btnActualizar;
    private Button btnBorrar;
    private Button btnCerrar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.producto_item);

        productosDb = new ProductosDb(this);
        etCodigo = findViewById(R.id.etCodigo);
        etNombre = findViewById(R.id.etNombre);
        etMarca = findViewById(R.id.etMarca);
        etPrecio = findViewById(R.id.etPrecio);
        rdbPerecedero = findViewById(R.id.rdbAuxiliar);
        rdbNoPerecedero = findViewById(R.id.rdb);
        btnBuscar = findViewById(R.id.btnBuscar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnRegresar = findViewById(R.id.btnRegresar);


        clearFields();

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoStr = etCodigo.getText().toString();
                if (!codigoStr.isEmpty()) {
                    Producto producto = productosDb.getProducto(codigoStr);
                    if (producto != null) {
                        etNombre.setText(producto.getNombre());
                        etMarca.setText(producto.getMarca());
                        etPrecio.setText(producto.getPrecio());
                        if (producto.getTipo().equals("Perecedero")) {
                            rdbPerecedero.setChecked(true);
                        } else {
                            rdbNoPerecedero.setChecked(true);
                        }
                    } else {
                        Toast.makeText(ProductoItem.this, "No se encontró el producto", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProductoItem.this, "Ingrese un código", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoStr = etCodigo.getText().toString();
                String nombre = etNombre.getText().toString();
                String marca = etMarca.getText().toString();
                String precioStr = etPrecio.getText().toString();

                if (!codigoStr.isEmpty()) {
                    int codigo = Integer.parseInt(codigoStr);
                    double precio = Double.parseDouble(precioStr);
                    boolean perecedero = rdbPerecedero.isChecked();

                    Producto producto = new Producto(codigoStr, nombre, marca, precioStr, -1, perecedero ? "Perecedero" : "No perecedero");

                    long updatedRows = productosDb.updateProducto(producto);
                    if (updatedRows > 0) {
                        Toast.makeText(ProductoItem.this, "Actualizado con éxito", Toast.LENGTH_SHORT).show();
                        clearFields();
                    } else {
                        Toast.makeText(ProductoItem.this, "Ha ocurrido un error, inténtalo de nuevo", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProductoItem.this, "Ingrese un código", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoStr = etCodigo.getText().toString();
                if (!codigoStr.isEmpty()) {
                    int codigo = Integer.parseInt(codigoStr);

                    // Eliminar el producto con el código proporcionado
                    productosDb.deleteProducto(codigo);

                    clearFields();
                    Toast.makeText(ProductoItem.this, "Eliminado correctamente", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProductoItem.this, "Ingrese un código para borrar", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialog();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoStr = etCodigo.getText().toString();
                String nombre = etNombre.getText().toString();
                String marca = etMarca.getText().toString();
                String precioStr = etPrecio.getText().toString();

                if (!codigoStr.isEmpty()) {
                    int codigo = Integer.parseInt(codigoStr);
                    double precio = Double.parseDouble(precioStr);
                    boolean perecedero = rdbPerecedero.isChecked();

                    Producto producto = new Producto(codigoStr, nombre, marca, precioStr, -1, perecedero ? "Perecedero" : "No perecedero");

                    long updatedRows = productosDb.updateProducto(producto);
                    if (updatedRows > 0) {
                        Toast.makeText(ProductoItem.this, "Actualizado con éxito", Toast.LENGTH_SHORT).show();
                        clearFields();
                    } else {
                        Toast.makeText(ProductoItem.this, "Ha ocurrido un error, inténtalo de nuevo", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProductoItem.this, "Ingrese un código", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    private void clearFields() {
        etCodigo.setText("");
        etNombre.setText("");
        etMarca.setText("");
        etPrecio.setText("");
        rdbPerecedero.setChecked(false);
        rdbNoPerecedero.setChecked(false);
    }

    private void showConfirmationDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Cerrar aplicación");
        builder.setMessage("¿Estás seguro de salir?.");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }
}