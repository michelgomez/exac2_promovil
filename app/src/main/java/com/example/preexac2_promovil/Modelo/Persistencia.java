package com.example.preexac2_promovil.Modelo;

import com.example.preexac2_promovil.Producto;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertProducto(Producto producto);
    public long updateProducto(Producto producto);
    public void deleteProducto(int id);
}
