package com.example.preexac2_promovil.Modelo;

import android.database.Cursor;

import com.example.preexac2_promovil.Producto;

import java.util.List;

public interface Proyeccion {
    public Producto getProducto(String codigo);
    public List<Producto> allProductos();
    public Producto readProducto(Cursor cursor);
}